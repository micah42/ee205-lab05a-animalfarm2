///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file palila.cpp
/// @version 1.0
///
/// Exports data about all palila birds
///
/// @author Micah Chinen <micah42@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   26_FEB_2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "palila.hpp"

using namespace std;

namespace animalfarm {

Palila::Palila( string newWhereFound, enum Color newColor, enum Gender newGender ) {
   gender = newGender;
   species = "Loxioides bailleui";
   featherColor = newColor;
   isMigratory = false;
   whereFound = newWhereFound;
}

/// Print out Palila ... then print whatever information Bird holds.
void Palila::printInfo() {
   cout << "Palila" << endl;
   cout << "   Where Found = [" << whereFound << "]" << endl;
   Bird::printInfo();
}

} // namespace animalfarm

