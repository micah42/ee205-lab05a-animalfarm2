///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nunu.cpp
/// @version 1.0
///
/// Exports data about all nunu fish
///
/// @author Micah Chinen <micah42@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   19_FEB_2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "nunu.hpp"

using namespace std;

namespace animalfarm {

Nunu::Nunu( bool newIsNative, enum Color newColor, enum Gender newGender ) {
   gender = newGender;
   species = "Fistularia chinensis";
   scaleColor = newColor;
   favoriteTemp = 80.6;
   isNative = newIsNative;
}

/// Print out Nunu ... then print whatever information Fish holds.
void Nunu::printInfo() {
   cout << "Nunu" << endl;
   cout << "   Is native = [" << boolalpha << isNative << "]" << endl;
   Fish::printInfo();
}

} // namespace animalfarm
