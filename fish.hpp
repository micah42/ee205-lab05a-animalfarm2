///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file fish.hpp
/// @version 1.0
///
/// Exports data about all fish
///
/// @author Micah Chinen <micah42@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   19_FEB_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "animal.hpp"

namespace animalfarm {

class Fish : public Animal {
public:
   enum Color   scaleColor;
   float        favoriteTemp;

   void printInfo();
   virtual const string speak();
};

} // namespace animalfarm
