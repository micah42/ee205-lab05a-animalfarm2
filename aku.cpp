///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file aku.cpp
/// @version 1.0
///
/// Exports data about all aku fish
///
/// @author Micah Chinen <micah42@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   20_FEB_2020
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "aku.hpp"

using namespace std;

namespace animalfarm {

Aku::Aku( float newWeight, enum Color newColor, enum Gender newGender ) {
   gender = newGender;
   species = "Katsuwonus pelamis";
   scaleColor = newColor;
   favoriteTemp = 75;
   weight = newWeight;
}

/// Print out Aku ... then print whatever information Fish holds.
void Aku::printInfo() {
   cout << "Aku" << endl;
   cout << "   Weight = [" << weight << "]" << endl;
   Fish::printInfo();
}

} // namespace animalfarm
