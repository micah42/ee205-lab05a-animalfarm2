///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nene.hpp
/// @version 1.0
///
/// Exports data about all nene
///
/// @author Micah Chinen <micah42@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   26_FEB_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

#include "bird.hpp"

using namespace std;


namespace animalfarm {

class Nene : public Bird {
public:
   string tagID;

   Nene( string newTagID, enum Color newColor, enum Gender newGender );

   void printInfo();

   virtual const string speak();
};

} // namespace animalfarm

